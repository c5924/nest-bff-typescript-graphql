import { User } from "src/users/entities/user.entities";

export default class Mock{
    static data = [
        {
            "id":1,
            "name":"Leonardo Pasquarelli",
            "active":true,
            "email":"leonardo.pasquarellif@gmail.com"
        },
        {
            "id":2,
            "name":"Fulano Teste",
            "active":true,
            "email":"fulano.teste@gmail.com"
        }
    ]

    static getData() {
        return this.data;
    }

    static updateData(User) {
        this.data = User;
    }
}