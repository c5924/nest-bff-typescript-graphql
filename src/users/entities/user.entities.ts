import { ObjectType, Field, Int} from '@nestjs/graphql'

@ObjectType()
export class User {
    @Field(() => Int, { description: 'User ID'})
    id: number;

    @Field(() => String, { description: 'User Nome'})
    name: string;

    @Field(() => Boolean, { description: 'Active'})
    active: boolean;

    @Field(() => String, { description: 'User email'})
    email: string;

} 