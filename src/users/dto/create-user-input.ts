import {InputType, Field, Int,PartialType} from '@nestjs/graphql'
import { CreateUserInput } from './update-user-input';

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput){

    @Field(() => Int)
    id:number;

    @Field(() => String, {description:'User Name'})
    name:string;

    @Field(() => Boolean, {description:'Active'})
    active:boolean;

    @Field(() => String, {description:'User Email'})
    email:string;
    
}