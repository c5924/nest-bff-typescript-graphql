import {InputType, Field, Int} from '@nestjs/graphql'

@InputType()
export class CreateUserInput{
    @Field(() => String, {description:'User Name'})
    name:string;

    @Field(() => Boolean, {description:'Active'})
    active:boolean;

    @Field(() => String, {description:'User Email'})
    email:string;
    
}