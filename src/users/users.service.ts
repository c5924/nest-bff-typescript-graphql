import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RESTDataSource } from '@apollo/datasource-rest'
import axios from 'axios';
import { User } from './entities/user.entities';
import Mock from '../data/users/users'
import { CreateUserInput } from './dto/update-user-input';
import { UpdateUserInput } from './dto/create-user-input';

@Injectable()
export class UsersService extends RESTDataSource{
  constructor() {
    super();
    this.baseURL = process.env.BFF_BASE_URL
  }

  async findOne(id: number): Promise<User> {
    try{
      //const response = this.get(`users/${id}`);
      const response = Mock.data.find(user => user.id === id);

      return response;      
    }catch(err: any){
      throw new Error(err)
    }
  }

  async findAll(): Promise<[User]> {
    try{
      //const response = this.get('users');
      const response: any = Mock.data;

      return response;   
    }catch(err: any){
      throw new Error(err)
    }
  }

  async add(createUserInput: CreateUserInput): Promise<User> {
    try{
      //const response = this.get('users');

      const id: number = Mock.data.length + 1

      const data = Mock.data.push({
        id,
        ...createUserInput
      });

      if(!data)
        throw new Error('cannot create user')

      const response = Mock.data.find(user => user.id === data);

      return response;      
    }catch(err: any){
      throw new Error(err)
    }
  }

  async update(id: number, updateUserInput: UpdateUserInput): Promise<User>{
    try{
      //const response = this.get('users');     

      const userExists: boolean = Mock.data.some(user => user.id === id);
      
      if(!userExists)
        throw new Error('user not found')

      const response = Mock.data.map(user => {
        if(user.id === id){
          user = {...updateUserInput}
        }
        return user
      })

      Mock.updateData(response)

      console.log('response',response);

      return Mock.data.find(user => user.id === id);      
    }catch(err: any){
      throw new Error(err)
    }
  }

  async disable(id: number): Promise<User>{
    try{
      const userExists: boolean = Mock.data.some(user => user.id === id);
      
      if(!userExists)
        throw new Error('user not found')

      const response = Mock.data.map(user => {
        if(user.id === id){
          user.active = false
        }
        return user
      })

      Mock.updateData(response)

      console.log('response',response);

      return Mock.data.find(user => user.id === id);       
    }catch(err: any){
      throw new Error(err)
    }
  }
}
