//IMPORTS
import { Resolver, Query, Mutation, Args, ID, Int } from '@nestjs/graphql';
//SERVICES
import { UsersService } from './users.service';
//ENTITIES
import { User } from './entities/user.entities';
//INPUT DATA
import { CreateUserInput } from './dto/update-user-input';
import { UpdateUserInput } from './dto/create-user-input';

@Resolver('User')
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  // query {
  //   getUsers {
  //     id
  //     name
  //     active
  //     email
  //   }
  // }
  
  @Query(() => [User], {name: 'getUsers'})
  async findAll() {
    return this.usersService.findAll();
  }

  // query {
  //   getUserById(id: 6) {
  //     id
  //     name
  //     email
  //     active
  //   }
  // }
  
  @Query(() => User, {name: 'getUserById'})
  async findOne(@Args('id',{ type: () => Int })id: number) {
    return this.usersService.findOne(id);
  }

  // mutation {
  //   addUser(
  //     createUserInput: {
  //       name: "Teste"
  //       email: "leonardo.fernandes@teste.com.br"
  //       active: true
  //     }
  //   ) {
  //     id
  //     name
  //     active
  //     email
  //   }
  // }
  @Mutation(() => User)
  async addUser(@Args('createUserInput') createUserInput: CreateUserInput): Promise<User> {
    return this.usersService.add(createUserInput)
  }

  // mutation {
  //   updUser(
  //     updateUserInput: {
  //       id: 4
  //       name: "Teste 2"
  //       email: "leonardo.fernandes@teste.com.br"
  //       active: true
  //     }
  //   ) {
  //     id
  //     name
  //     active
  //     email
  //   }
  // }
  @Mutation(() => User)
  async updUser(@Args('updateUserInput') updateUserInput: UpdateUserInput) {
    return this.usersService.update(updateUserInput.id,updateUserInput)
  }

  // mutation {
  //   disableUser(id: 2) {
  //     id
  //     name
  //     active
  //     email
  //   }
  // }
  @Mutation(() => User)
  async disableUser(@Args('id',{ type: () => Int }) id: number) {
    return this.usersService.disable(id)
  }
}

